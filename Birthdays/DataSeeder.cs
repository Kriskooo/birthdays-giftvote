﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Birthdays
{
    public class DataSeeder
    {
        private readonly VotingDbContext dbContext;

        public DataSeeder(VotingDbContext context)
        {
            dbContext = context;
        }

        public void SeedData()
        {
            if (!dbContext.Employees.Any())
            {
                // Create test employees
                var employees = new List<Employee>
                {
                    new Employee { Name = "Kris", BirthDate =  DateTime.SpecifyKind(new DateTime(1995, 2, 5), DateTimeKind.Utc), Username = "Krisko", Password = "kris" },
                    new Employee { Name = "Ivan", BirthDate = DateTime.SpecifyKind(new DateTime(1990, 1, 1), DateTimeKind.Utc), Username = "Vankata", Password = "test" },
                    new Employee { Name = "Gosho", BirthDate = DateTime.SpecifyKind(new DateTime(1991, 2, 2), DateTimeKind.Utc), Username = "Gogo", Password = "testing" },
                };

                // Create test gifts
                var gifts = new List<Gift>
                {
                    new Gift { Name = "Book" },
                    new Gift { Name = "Samsung S23" },
                    new Gift { Name = "Iphone 14" }
                };

                dbContext.Employees.AddRange(employees);
                dbContext.Gifts.AddRange(gifts);

                dbContext.SaveChanges();
            }
        }
    }
}
