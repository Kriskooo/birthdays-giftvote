﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Birthdays
{
    public class GiftVote
    {
        public int Id { get; set; }
        public Employee? BirthdayEmployee { get; set; }
        public List<int>? Voters { get; set; }

        [NotMapped]
        public IDictionary<int, int>? GiftVotes { get; set; }
        public int EmployeeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EndTime { get; set; }
        public int CreatorId { get; set; }
    }
}
