﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Birthdays
{
    public class VotingService
    {
        private readonly DatabaseRepository repo;

        public VotingService(DatabaseRepository repo)
        {
            this.repo = repo;
        }

        public void StartVote(int employeeId)
        {
            if (employeeId <= 0)
                throw new ArgumentException("Invalid employee ID.");

            if (HasActiveVoteFor(employeeId))
                throw new InvalidOperationException("There is already an active vote for this employee.");

            DateTime birthDate = GetEmployeeBirthDate(employeeId);
            int birthYear = birthDate.Year;
            if (HasVoteForYear(employeeId, birthYear))
                throw new InvalidOperationException("There is already a vote for this employee's birth year.");

            var vote = new GiftVote
            {
                EmployeeId = employeeId,
                IsActive = true,
                GiftVotes = InitializeGiftVotes(),
                CreatorId = employeeId // Set the creator of the vote
            };

            repo.AddVote(vote);

            Console.WriteLine("Vote started successfully!");
        }

        private DateTime GetEmployeeBirthDate(int employeeId)
        {
            Employee employee = repo.GetEmployeeById(employeeId);

            if (employee == null)
                throw new Exception("Employee Not Found!");

            return employee.BirthDate;
        }

        private bool HasActiveVoteFor(int employeeId)
        {
            return repo.GetActiveVotes()
                .Any(v => v.EmployeeId == employeeId);
        }

        private bool HasVoteForYear(int employeeId, int year)
        {
            return repo.GetVotes()
                .Any(v => v.EmployeeId == employeeId && v.CreatedDate.Year == year);
        }

        private Dictionary<int, int> InitializeGiftVotes()
        {
            var gifts = repo.GetGifts();
            var giftVotes = new Dictionary<int, int>();

            foreach (var gift in gifts)
            {
                giftVotes.Add(gift.Id, 0);
            }

            return giftVotes;
        }

        public void VoteForGift(int employeeId, int giftId)
        {
            if (!IsValidEmployee(employeeId))
                throw new Exception("Invalid employee ID.");

            if (!IsValidGift(giftId))
                throw new Exception("Invalid gift ID.");

            var vote = GetActiveVoteForBirthdayEmployee(employeeId);

            if (HasVoted(vote, employeeId))
                throw new Exception("You have already voted in this active vote.");

            AddVote(vote, employeeId, giftId);
            repo.UpdateVote(vote);
        }

        private void AddVote(GiftVote vote, int employeeId, int giftId)
        {
            vote.Voters.Add(employeeId);
            vote.GiftVotes[giftId]++;
        }

        private bool HasVoted(GiftVote vote, int employeeId)
        {
            return vote.Voters.Contains(employeeId);
        }

        private bool IsValidEmployee(int employeeId)
        {
            var employee = repo.GetEmployeeById(employeeId);
            return employee != null;
        }

        private bool IsValidGift(int giftId)
        {
            var gift = repo.GetGiftById(giftId);
            return gift != null;
        }

        private GiftVote GetActiveVoteForBirthdayEmployee(int employeeId)
        {
            var employee = repo.GetEmployeeById(employeeId);

            if (employee == null || !employee.IsBirthday)
                throw new Exception("Invalid operation: The employee is not valid or it's not their birthday.");

            return repo.GetActiveVotes()
                .FirstOrDefault(v => v.EmployeeId == employeeId);
        }

        public void EndVote(int voteId, int employeeId)
        {
            var vote = repo.GetVoteById(voteId);

            if (vote == null)
                throw new Exception("Vote not found.");

            if (vote.CreatorId != employeeId)
                throw new UnauthorizedAccessException("Not authorized to end this vote");

            vote.IsActive = false;
            vote.EndTime = DateTime.UtcNow;

            repo.UpdateVote(vote);

            ShowVoteResults(vote);
        }

        private void ShowVoteResults(GiftVote vote)
        {
            var resultCounts = vote.GiftVotes;
            var sortedResults = resultCounts.OrderByDescending(r => r.Value);

            Console.WriteLine("Vote results:");
            foreach (var result in sortedResults)
            {
                var gift = repo.GetGiftById(result.Key);
                Console.WriteLine($"{gift.Name} - {result.Value} votes");
            }

            Console.WriteLine("\nVoters:");
            foreach (var voterId in vote.Voters)
            {
                var voter = repo.GetEmployeeById(voterId);
                Console.WriteLine($"{voter.Name} ({voter.Username})");
            }

            var nonVoters = GetEmployeesWithoutVotes(vote);
            Console.WriteLine("\nNon voters:");
            foreach (var nonVoter in nonVoters)
            {
                Console.WriteLine($"{nonVoter.Name} ({nonVoter.Username})");
            }
        }

        private List<Employee> GetEmployeesWithoutVotes(GiftVote vote)
        {
            var employeeIdsWithVotes = vote.Voters;
            var allEmployees = repo.GetEmployees();

            return allEmployees
                .Where(e => !employeeIdsWithVotes.Contains(e.Id))
                .ToList();
        }
    }
}
