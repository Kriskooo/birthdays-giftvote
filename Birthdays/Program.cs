﻿using Birthdays;
using System;
using static System.Runtime.InteropServices.JavaScript.JSType;

public class Program
{
    public static void Main(string[] args)
    {
        using (var dbContext = new VotingDbContext())
        {
            var repo = new DatabaseRepository(dbContext);
            var votingService = new VotingService(repo);

            var dataSeeder = new DataSeeder(dbContext);
            dataSeeder.SeedData();

            try
            {
                var employeeId = 1;
                votingService.StartVote(employeeId);

                var giftId = 2;
                votingService.VoteForGift(employeeId, giftId);

                var voteId = 3;
                votingService.EndVote(voteId, employeeId);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }
    }
}
