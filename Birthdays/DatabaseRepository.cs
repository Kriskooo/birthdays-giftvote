﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Birthdays
{
    public class DatabaseRepository
    {
        private readonly VotingDbContext dbContext;

        public DatabaseRepository(VotingDbContext context)
        {
            dbContext = context;
        }

        public List<Employee> GetEmployees()
        {
            return dbContext.Employees.ToList();
        }

        public Employee GetEmployeeById(int id)
        {
            return dbContext.Employees.FirstOrDefault(e => e.Id == id);
        }

        public void AddEmployee(Employee employee)
        {
            dbContext.Employees.Add(employee);
            dbContext.SaveChanges();
        }
        public List<Gift> GetGifts()
        {
            return dbContext.Gifts.ToList();
        }
        public Gift GetGiftById(int id)
        {
            return dbContext.Gifts.FirstOrDefault(g => g.Id == id);
        }
        public void AddGift(Gift gift)
        {
            dbContext.Gifts.Add(gift);
            dbContext.SaveChanges();
        }

        public List<GiftVote> GetActiveVotes()
        {
            return dbContext.GiftVotes.Where(v => v.IsActive).ToList();
        }

        public GiftVote GetVoteById(int voteId)
        {
            return dbContext.GiftVotes.FirstOrDefault(v => v.Id == voteId);
        }

        public void AddVote(GiftVote vote)
        {
            dbContext.GiftVotes.Add(vote);
            dbContext.SaveChanges();
        }

        public void UpdateVote(GiftVote vote)
        {
            dbContext.GiftVotes.Update(vote);
            dbContext.SaveChanges();
        }

        public List<GiftVote> GetVotes()
        {
            return dbContext.GiftVotes.ToList();
        }

        public bool HasVoteForYear(int employeeId, int year)
        {
            return dbContext.GiftVotes.Any(v => v.EmployeeId == employeeId && v.CreatedDate.Year == year);
        }

        public void StartVote(GiftVote vote)
        {
            var existingVote = dbContext.GiftVotes.FirstOrDefault(v => v.BirthdayEmployee.Id == vote.BirthdayEmployee.Id && v.IsActive);
            if (existingVote != null)
            {
                throw new InvalidOperationException("There is already active voting for this birthday person.");
            }

            vote.IsActive = true;
            vote.CreatedDate = DateTime.Now;
            dbContext.GiftVotes.Add(vote);
            dbContext.SaveChanges();
        }

        public void EndVote(GiftVote vote)
        {
            if (!vote.IsActive)
            {
                throw new InvalidOperationException("End Vote!");
            }

            vote.IsActive = false;
            vote.EndTime = DateTime.Now;
            dbContext.SaveChanges();
        }

        public void AddVoteOption(VoteOption voteOption)
        {
            dbContext.VoteOptions.Add(voteOption);
            dbContext.SaveChanges();
        }

        public void UpdateVoteOption(VoteOption voteOption)
        {
            dbContext.VoteOptions.Update(voteOption);
            dbContext.SaveChanges();
        }
    }
}

