﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Birthdays
{
    public class VoteOption
    {
        public int Id { get; set; }
        public int VoteId { get; set; }
        public virtual GiftVote? GiftVote { get; set; }
        public int GiftId { get; set; }
        public virtual Gift? Gift { get; set; }
        public int VoteCount { get; set; }
    }
}
