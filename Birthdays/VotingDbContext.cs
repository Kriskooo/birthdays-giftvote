﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Birthdays
{
    public class VotingDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Gift> Gifts { get; set; }
        public DbSet<GiftVote> GiftVotes { get; set; }
        public DbSet<VoteOption> VoteOptions { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseNpgsql("Host=localhost;Database=giftvotingdb;Username=postgres;Password=Krisi123456");
    }
}

